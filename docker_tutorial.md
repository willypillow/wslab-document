# 工作站 Docker 使用方式

### Step 1
啟用 systemd 的 linger 功能與註冊 subuid / subgid range：

```
$ linger-switch enable
$ subuid-register
```

### Step 2
安裝 rootless docker：
```
$ curl -fsSL https://get.docker.com/rootless | sh
```

### Step 3
設定 `DOCKER_HOST` 與 `PATH` 環境變數：
```
$ export PATH=$HOME/bin:$PATH
$ export DOCKER_HOST=unix:///run/user/$(id -u)/docker.sock
```
此兩行設定可以在上一步指令輸出的最後面找到：
```
# Docker binaries are installed in [...]
which: no dockerd in ([PATH...])
# WARN: dockerd is not in your current PATH or pointing to [...]/dockerd
# Make sure the following environment variables are set (or add them to ~/.bashrc):\n
export PATH=[...]:$PATH
export DOCKER_HOST=unix:///run/user/[uid]/docker.sock
```
若輸出是如此代表 `PATH` 已有設定，只需在設定 `DOCKER_HOST` 即可：
```
# Docker binaries are installed in [...]
# Make sure the following environment variables are set (or add them to ~/.bashrc):\n
export DOCKER_HOST=unix:///run/user/[uid]/docker.sock
```

亦可將此設定放入慣用的 shell 設定檔（例如 `~/.bashrc`）當中。

### Step 4
由於 docker 的資料可能會占用較大空間，建議將 docker 的資料路徑放到 `/tmp2` 下：
編輯 `~/.config/systemd/user/docker.service`，將 `ExecStart=` 一行開頭的指令最後加上 `--data-root=[SOME_PATH]` 以設定 docker 資料路徑。設定完畢後，重新啟動 docker daemon：
```
$ systemctl --user daemon-reload
$ systemctl --user restart docker
```

### Step 5
安裝完畢，可使用 docker 相關指令，並可使用 `systemctl --user start docker`、`systemctl --user stop docker` 開始／停止 docker daemon。

### 注意事項


- rootless 版本的 docker 功能上有所限制（詳見 [docker 官方網頁](https://docs.docker.com/engine/security/rootless/#known-limitations)）。
- 若要將 docker 安裝在多台工作站上，則每台工作站上皆須執行 1. 與 3. 兩個步驟，另外需要自行讀取 systemd 設定檔並啟用 docker daemon（`systemctl --user daemon-reload; systemctl --user start docker`）。
