﻿# 軟體 Software
各工作站皆有安裝為數不少的軟體供大家使用，若有不足，請寄信至 ta217@csie.ntu.edu.tw 申請安裝。

查看工作站安裝了什麼軟體，可以用下面指令：

ArchLinux (linux, oasis):
```
pacman -Qe
```
FreeBSD (bsd1)
```
pkg info
```
或者你也可以參考[monitor](https://monitor.csie.ntu.edu.tw) 的 Packages 頁面。需要注意的是，工作站管理團隊不負責安裝如 python package 等語言套件，如有需求請參考[Python Package 安裝教學](./python_package_installation_tutorial.md)。 
除此之外，工作站有安裝版權軟體 MathWorks MATLAB，你可以在進入 MATLAB 後用 ``ver`` 指令查看已安裝的 Toolbox。
