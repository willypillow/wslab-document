﻿# 使用 VNC Using VNC
如果你需要使用圖形化介面(e.g. MATLAB)，你可以使用 VNC：

## step 0
請在`~/.vnc/xstartup`檔案設定你要啟動的桌面環境。如果你不知道要怎麼設定，請使用以下的script啟動GNOME桌面環境，或者參考[Arch Wiki](https://wiki.archlinux.org/index.php/TigerVNC#Edit_the_startup_script)。
```bash
#!/bin/sh
unset SESSION_MANAGER
unset DBUS_SESSION_BUS_ADDRESS
exec dbus-launch gnome-session
```

## step 1
登入工作站後，執行 ``vncserver`` 來開啟新桌面；接著你可以看到如下的資訊：
```
New 'linux1:2 (xxxxxxxx)' desktop is linux1:2

Starting applications specified in /the/path/of/your/home/.vnc/xstartup
Log file is /the/path/of/your/home/.vnc/linux1:2.log
```
## step 2

如果你過去從未使用過 VNC，此時系統將會要求你設定一組密碼；請儘量設定高強度的密碼，並牢記它。

## step 3

接著，請打開剛剛訊息中的 log file （通常為 $HOME/.vnc/linux{}:{}.log)；在該檔案中你能看到 VNC 建立 connection 所使用的 port：

```
Xvnc TigerVNC 1.8.0 - built Jan 25 2018 12:24:55
Copyright (C) 1999-2017 TigerVNC Team and many others (see README.txt)
See http://www.tigervnc.org for information on TigerVNC.
Underlying X server release 11906000, The X.Org Foundation


Thu Feb 22 18:54:47 2018
 vncext:      VNC extension running!
 vncext:      Listening for VNC connections on all interface(s), port 5902
 vncext:      Listening for HTTP connections on all interface(s), port 5802
 vncext:      created VNC server for screen 0
```

此段訊息代表目前 VNC 正使用 port 5902 作為 connection port。

## step 4

由於 VNC 為明碼傳輸，直接連線有高度風險；在此我們建議使用者利用 SSH tunnel 來進行連線：

```
$ ssh -NfL 12345:localhost:5902 linux1.csie.ntu.edu.tw
```

此指令會幫你建立一條由本機的 port 12345 到 linux1 的 port 5902 之連線。

## step 5
此時你可以打開你的 VNC client (e.g. vncviewer)；server 輸入 localhost (即本機)，port 輸入你方才設定的數字，這樣便能安全地使用 VNC 服務了。
