# 帳號解鎖/密碼重置  Account Unlock/Password Reset

**請注意: 為了確保安全性，請遵守下列的申請方式。**<br>
**Note: In order to ensure security, please follow the instructions given below.**

以下任一種方法可以申請解鎖/密碼重設。<br>
Choose one of the methods listed below to request account unlock and password reset.

1.
   * 下載申請單（[申請單](https://ppt.cc/fwCucx)）。<br>
   Download application（[Application](https://ppt.cc/fwCucx)）<br>
   * 透過臺大 email (學號@ntu.edu.tw) 或掃描學生證/身分證/校友證/畢業證書等身分證明文件，寄信給系統管理助教群提出申請。<br>
   Send a mail from NTU Mail (student_id@ntu.edu.tw) or scan your NTU ID/Diploma and dend to the System Management TAs.<br>
   * 信件主題 Mail subject:  **[Reset Password] USERNAME**<br><br>

2. 
   * 下載申請單（[申請單](https://ppt.cc/fwCucx)）。<br>
   Download application （[Application](https://ppt.cc/fwCucx)）<br>
   * 攜帶學生證/教職員證/校友證/畢業證書，當面向系統管理助教群提出申請。<br>
   Show your NTU ID/Diploma to any member of System Management TAs in person.<br>
