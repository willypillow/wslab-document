﻿

# 工作站帳號使用規範 CSIE Account Rules


### 申請資格

本系教職員生提出身分證明文件後（e.g. 教職員證、學生證、修課證明、雇用契約書）皆可提出申請，本系學生於入學時，帳號即已自動建立，無須另外申請。

### 申請方式

親臨工作站教學實驗室辦理

### 帳號使用期限

* 學生：
	* 本系畢業學生（僅限資工系所、網媒所及人工智慧碩士班）可選擇永久保留，退學者將無條件刪除。
	* 逕博學生將移除原碩班帳號。
	* 外系修課帳號將於每學期結束後刪除。
	* 雙主修及輔系生比照外系修課帳號辦理。
* 教授：
	* 退休後永久保留，離職後刪除。
* 職員（含工讀生）：
	* 退休後永久保留，離職後刪除。
* 博士後研究員、助理、課程助教：
	* 聘期結束刪除。
* 217助教及NASA團隊成員：
	* 經實名制登記後，永久保留。

### 帳號使用範圍

* CSIE Workstation
* CSIE WIFI
* CSIE G-Suite <small><b>（限本系）</b></small>
* CSIE VPN <small><b>（限本系）</b></small>

### 帳號使用規則

帳號使用限作教學及研究用途，若利用帳號進行營利、妨礙風化、侵入他人電腦與濫用網路資源、觸發資訊安全之情事（e.g. 發送大量垃圾信件、架設釣魚網站），將視情節給予帳號停權或刪除之處分。

停權規範採累加罰則制：初犯者罰一個月停權，第二次三個月，第三次永久停權；若情節重大，得立即處以永久停權或帳號刪除。

### 帳號凍結規範

為防範帳號長期未使用而產生資安風險，若永久保留之帳號於一年內未進行工作站登入且無法透過電子郵件聯絡帳號持有者，將進行帳號凍結。

凍結帳號將無法使用任何服務，若須解除鎖定，[請依照流程填寫申請表](https://wslab.csie.ntu.edu.tw/request_password_reset.html)並透過NTU信箱（學號@ntu.edu.tw）寄給系統管理助教提出解鎖申請；如NTU信箱失效，可檢附相關證明文件（e.g. 校友證、畢業證書）辦理。

辦理帳號解鎖須同意開啟Google帳號二階段認證（強制綁定個人信箱及連絡電話），以加強帳號管理及安全(FYI, [Account Unlock/Password Reset](https://wslab.csie.ntu.edu.tw/request_password_reset.html))。

<br/>
- - -
<br/>

## 工作站使用規範 Computation & Usage Rules 

### rule 1

本系所有工作站皆限制作教學與研究用途（包含本系師生之一般使用，e.g. 收信）；其中
* linux1~linux15 主要為支援教學與研究上短期需要大量運算者使用。
  * 請斟酌在每台機器上所執行的程式，不會影響其他使用者的使用權利。
  * 執行需要大量資源之程式時，請隨時注意程式執行的狀態，避免影響到其他使用者的使用權。
  * 在工作站執行數小時以上的程式時，請使用有意義的程式名稱，勿使用 a.out 等預設檔名，以方便識別該程式的用途。
  * 工作站管理團隊可能於機器無法負荷時停止使用者之大型程式，並寄信通知該使用者，還請見諒。
  * 細則
     * 平時（期中考與期末考前後一週之外的時間），每人在工作站上的 CPU 總用量以 10000% 為限， memory 總用量以 200G 為限；此限制只在工作站滿載時生效，作為停止 process 的依據。
     * 非平時，每人的 CPU 總用量以 7500% 為限，memory 以 150G 為限。
     * 若 process 經工作站管理團隊評估有異常使用狀況者（e.g. CPU utilization 過高），則不在上述細則範圍內，會直接停止該 process。
* oasis1~oasis3 與 bsd1 主要為提供本系師生之一般使用，且不需大量運算者使用，e.g. SSH, BBS, IRC, 收信等。於這些機器執行大型 / 需要大量運算資源之程式者將一律停止該程式之運行（SIGKILL) 。
* 工作站家目錄空間限制
  * 因工作站空間有限，使用者家目錄之空間使用限制如下:
    * 大學部容量限制為 2G
    * 碩博士班容量限制為 5G
    * 系友為 200M
  * 倘若超過使用限制，系統將會讓您的家目錄進入Read-only的狀態。如有使用大型檔案的需求，請善用各工作站之本機空間(/tmp2)，以遵守家目錄空間使用規範，並確保存取速度。(FYI, [Tmp2 Usage Rules](https://wslab.csie.ntu.edu.tw/tmp2_usage_rules.html))
* Fail2ban 規則
  * 為了確保安全性，若一個 IP 位址在十分鐘內登入失敗 6 次，將會被封鎖 24 個小時。
  * 由於台大校內的 IP 在白名單上，即便您在一台機器上被封鎖，還是能夠藉由其他工作站來存取該機器。
  * 若您仍然需要提前解除封鎖，請寄信至工作站管理團隊。

### rule 2
>>>>>>> Stashed changes
根據臺灣學術網路管理規範第九條第一項規定，本系所有工作站禁止營利行為（e.g. 挖掘虛擬貨幣），違者將處以帳號永久停權之處分。
### rule 3
根據臺灣學術網路管理規範第九條第二項規定，本系所有工作站禁止存取影響兒童及青少年身心健全發展，妨害風化之資訊（e.g. 色情、猥褻之影片），違者將直接刪除該檔案，必要時得處以帳號停權之處分。
### rule 4
根據臺灣學術網路管理規範第九條第三至七項規定，本系所有工作站禁止任何非法取得他人資訊或侵入他人電腦之行為，亦禁止任何濫用網路資源之情事（e.g. 發送大量垃圾郵件、架設釣魚網站），違者將視情節給予一定之處分（帳號停權）。
   * 採累加罰則：第一次犯者罰一個月停權，第二次三個月，第三次永久停權，若情節重大，得立即處以永久停權
### rule 5
根據臺灣學術網路管理規範第十條規定，本系所有工作站不得作為可能涉及侵害網路智慧財產權之使用（e.g. 下載盜版作品，使用盜版軟體），違者將直接刪除相關檔案，必要時得處以帳號停權之處分。
### rule 6
違反上述二至五項規範者，使用者皆應依相關法令規定自負法律責任。

<!--7. 禁止在工作站上使用 P2P 服務，違者將直接停止該程式，必要時得處以帳號停權之處分。-->

<br/>

_以上所有規範經本系相關會議修改通過，2020年11月12日公告，並於同日生效。_

<br/>

