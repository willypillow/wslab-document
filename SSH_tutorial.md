# SSH 教學 SSH Tutorial

If it's your first time using "ssh" to connect to an SSH server (e.g. our workstations), here's a brief tutorial for Windows, MacOS and Linux users.

## Windows
### Windows 10
If you're now using Windows 10 (with version newer than 1607 "Anniversary Update"), you can directly activate the Windows Subsystem for Linux. Afterwards, you can follow the guides below for MacOS / Linux.
Guideline for activating WSL: [https://docs.microsoft.com/en-us/windows/wsl/install-win10](https://docs.microsoft.com/en-us/windows/wsl/install-win10)<br>
**Note:** this guideline is for Windows 10 later than 1709 "Fall Creators Update".

### Other
If you're using older versions of Windows, you may use PuTTY to connect to the server; otherwise, Cygwin is a large collection of GNU and Open Source tools, which is another choice for you to use the ssh.

For PuTTY, please refer to here: [https://mediatemple.net/community/products/dv/204404604/using-ssh-in-putty-](https://mediatemple.net/community/products/dv/204404604/using-ssh-in-putty-)
<br>For Cygwin, please refer to here:
[https://www.howtogeek.com/howto/41560/how-to-get-ssh-command-line-access-to-windows-7-using-cygwin/](https://www.howtogeek.com/howto/41560/how-to-get-ssh-command-line-access-to-windows-7-using-cygwin/)

## MacOS / Linux
If you're using MacOS / Linux, you can open the terminal app, and key in the command as:
```
ssh [username]@[host]
```
For example, 
```
ssh b00902000@linux1.csie.ntu.edu.tw
```
If it's your first time connecting to the machine, you will be asked to confirm the identity of it, type yes; then you will be prompted to enter your password. After entering your password, you will be connected to the server through SSH.

Here are the ECDSA host key for each workstation (you can copy-paste to ```~/.ssh/known_hosts```): [file](_static/known_hosts) 

Before you start using workstations, please read the [computation and usage rules](https://wslab.csie.ntu.edu.tw/computation_and_usage_rules.html) first; we also list some [tips for workstation usage](https://wslab.csie.ntu.edu.tw/tips_for_workstation_usage.html) which you might be interested in.
