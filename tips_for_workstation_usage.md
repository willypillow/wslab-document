﻿# 工作站使用小技巧 Tips for Workstation Usage
1. 在使用工作站前，請詳閱工作站使用規範。
2. 查看工作站狀態
    * 本系目前共有 linux1~linux15, oasis1~oasis3, bsd1, meow1-meow2 等工作站，你可以在 monitor.csie.ntu.edu.tw 查看各台工作站當前之狀態（CPU, memory, swap, e.t.c.)，決定你要使用哪一台。
    * 在 monitor.csie.ntu.edu.tw 中，你也可以查看目前工作站安裝的套件列表；如果有任何套件上的需求，歡迎寄信至工作站管理團隊（ta217@csie.ntu.edu.tw）詢問。
3. 善用 /tmp2
    * 本系所有工作站都提供了 /home 與 /tmp2 兩個檔案系統供使用者儲存資料；其中 /home 是所有工作站共用的 NFS，i.e. 在 /home 做的操作會在所有工作站間同步，而 /tmp2 是每台工作站本機的儲存空間，提供使用者儲存大量資料。
    * 由於 NFS 效率較差，且家目錄配額有限，如果有儲存大量資料，跑大量 I/O 的程式，或者不需在工作站間同步的使用需求，建議你多加利用 /tmp2 以達到更好的使用效能。
    * 使用 /tmp2 時請不要將檔案直接放至目錄底下；請先創建一個名稱可辨識的目錄（e.g. 學號），接著將你需要使用的資料放至該目錄下。需要注意的是，如果你不希望別人看見你的資料，請記得將目錄權限設定好，這方面工作站管理團隊不負任何管理上的責任，一切由使用者決定。
    * /tmp2 本質上是暫存空間，工作站管理團隊可能在機器下線 / 重灌 / 升級時將資料清空，每學期也可能一到兩次清空 /tmp2，使用者必須自負保管資料之責任。
    * /tmp2 的使用上請務必遵守 tmp2 使用規則。
4. htdocs
    * 你可以在個人家目錄底下使用（創建）htdocs 目錄，將網頁放在該目錄下，便可以使用 csie.ntu.edu.tw/~[username]/[filename] 瀏覽該網頁了。
    * index.html 為預設之讀取檔案。
5. oasis*
    * 如果你想在工作站上跑一些小程式，可以多加利用 oasis* ；這幾台工作站專門提供系上師生之一般使用，如果有使用者在上頭跑高負載的程式，會被工作站管理團隊砍掉。
6. screen / tmux
    * 如果你需要跑執行時間較長，或者同時多個程式，你可以使用 screen 與 tmux 這兩個套件；這兩個套件可以讓你簡單開啟多個工作環境（session），就不需要反覆登入同一台工作站了；同時，你也可以藉由將 session detach 來跑執行時間較長的程式，不需要一直維持連線。
7. ps / htop
    * 在使用工作站時，你可能會產生一些 zombie process 或持續執行（e.g. 掉入無窮迴圈）的程式；你可以使用 ps 或 htop 指令來查看相關資訊，並將自己產生的 zombie 清除掉，避免消耗工作站不必要的資源:
      * 一般情況下，你可以利用 `top` 與 `htop` 這兩個指令監控自己的 process 及工作站的記憶體、CPU負載等資訊；除此之外，你也可以使用 `ps -u $username` ($username 為你的工作站帳號) 列出目前自己跑的所有 process，或進一步使用 `pstree -u [username]` 來監測自己的程式 fork 子程式的情況。
      * 當你發現自己產生了 zombie process，你可以先用上述的幾個指令確定 process 的 pid，接著使用 `kill -9 $pid` ($pid 為該 process 的 pid) 來砍掉自己的 process；如果你確定你要砍掉的 process 都有同樣的名稱或格式，也可以使用 `pkill -u $username $pattern` （$pattern 為你想要指定的名稱或格式）來砍掉符合條件的 process。
      * 如果你想要進一步知道自己的 process 到底執行了哪些操作（system call），可以使用 `strace $program` ($program 為你想要跑的程式) 或 `strace -p $pid` 來追蹤產生的 process。
      * 若你真的無法砍掉自己產生的 process，請儘速來信至 ta217@csie.ntu.edu.tw，工作站管理團隊會協助你處理相關事項。
8. 使用 ulimit 控制 process 與 file descriptor 的上限 
    * 工作站上有設定 process / session 的部份資源上限（e.g. 可以開的 file descriptor 數量），你可以使用 ulimit (-a) 指令來查看相關資訊；如果你正在進行撰寫系統相關程式、批改作業等可能會超過資源上限的行為(e.g. 可能會 fork 子程式的 code），可以嘗試降低 soft limit 來避免意外狀況發生（e.g. 無法登入工作站）。 
    * 你可以參考 ulimit 的 manual 來了解相關的參數設定：``man ulimit``。
9. 使用 systemd-run 控制 CPU 與 memory 用量的上限 
    * 如果你有限制 CPU 或 memory 的需求，可以使用 systemd-run 來調整上限；這樣做也能有效降低工作站硬體的負擔，如果你沒有迫切需求的話可以固定做設定，以下是簡單的範例：``systemd-run --user --scope -p MemoryLimit=3G -p CPUWeight=512 -p IOWeight=500``
    * 其中 `MemoryLimit` 限制 memory usage，`CPUWeight` 限制 CPU 用量，`IOWeight` 限制 IO bandwidth 的使用量。
    * 你可以參考這裡來了解相關的參數設定：[https://www.freedesktop.org/software/systemd/man/systemd.resource-control.html](https://www.freedesktop.org/software/systemd/man/systemd.resource-control.html)

10. ssh key
    * 如果你不想每次登入時都要打密碼，可以考慮改用 ssh key 來登入工作站。
    * 教學： [https://www.digitalocean.com/community/tutorials/how-to-set-up-ssh-keys--2](https://www.digitalocean.com/community/tutorials/how-to-set-up-ssh-keys--2)
    * 請注意，ssh private key 是非常重要的個人隱私，不要隨意將 private key 交予他人；同時也建議你務必要設定 keyphrase，再利用 ssh agent 登入，這樣你一樣不用打密碼，同時會有更好的安全性。
11. 使用其他的 port 登入
    * 有些網路防火牆會擋下 port 80 與 443 以外的連線，i.e. 假設你只使用 http / https 上網；在這種情況下將無法直接登入工作站；此時你可以用 `ssh -p [80/443] [hostname]` 來登入。
12. 更改預設的 shell
    * 如果你想使用 bash 以外的 shell，可以在工作站使用 `chsh` 指令來更換登入預設的 shell。目前工作站提供了 bash, csh, ksh, zsh 等選項，你可以使用該指令詳細查看；此設定會在所有工作站間同步，還請注意。
