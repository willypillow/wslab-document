# 申請表單 Application Forms

[臺大資工系帳號申請單（教師、同仁、外系選修、研究助理、助教、博士後研究）](https://drive.google.com/file/d/1IU6O9D-lNWxOYKrFrL1PHhpRxYdipBLi/view?usp=sharing)<br>
[Account Application (Faculty, Staff, Inter-dept. students, RA, TA, Postdoc)](https://drive.google.com/file/d/1nsWEGVzriAeTaEYgIVNa3mB0CfqpZBXf/view?usp=sharing)<br>
[資訊學群主機代管機房空間申請單](https://drive.google.com/file/d/1z0w6bzYTD44rgI-RS96rLfECSz9ozoDG/view?usp=sharing)<br>
[資訊學群主機代管機房空間管理人員申請單](https://drive.google.com/file/d/1HeuPUo8LhDHARSdnue4iwzpBgjNx5uvZ/view?usp=sharing)
