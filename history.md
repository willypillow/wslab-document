# History

## 歷任/兼任助教

```eval_rst
.. csv-table:: 
   :header: "年代", "助教/兼任助教", "指導教授"
   :widths: 15, 40, 20

   1991-1992, 沈玉麟, 許永真教授 (yjhsu)
   1992-1994, 陳瑞華 (TA), 項潔教授 (hsiang)
   1994-1995, 黃怡誠 (ychuang), 項潔教授 (hsiang)
   1995-1996, 周承復 (ccf), 項潔教授 (hsiang)
   1996-1997, 蘇峻平 (sjp), 項潔教授 (hsiang)
   1997-1998, 莊永裕 (cyy), 項潔教授 (hsiang)
   1998-1999, 李維源 (wing), 林智仁教授 (cjlin)
   1999-2000, 翁文彥 (weng) 陳致偉 (zwchen), 林智仁教授 (cjlin)
   2000-2000, 吳明龍 (jeromewu) 陳致緯 (zwchen), 林智仁教授 (cjlin)
   2000-2001, 廖愷修 (kaai) 歐昱言 (yien), 林智仁教授 (cjlin)
   2001-2002, 許惠玲 (ling) 歐昱言 (yien), --
   2002-2003, 莊靜華 (claire) 歐昱言 (yien), 周承復教授 (ccf)
   2003-2004, 歐昱言 (yien) 林弘德 (piaip), 周承復教授 (ccf)
   2004-2005, 黃鈺峰 (yfhuang) 吳光哲 (kcwu), 周承復教授 (ccf)
   2005-2005, 黃鈺峰 (yfhuang) 張琮翔 (vgod) 范榮恩 (rafan), 朱浩華教授 (hchu)
   2005-2006, 黃鈺峰 (yfhuang) 林東昀 (toung) 黃子桓 (tzhuan), 朱浩華教授 (hchu)
   2006-2006, 吳延年 (sam) 林東昀 (toung) 蔡宓真 (eaglet), 朱浩華教授 (hchu)
   2006-2007, 吳延年 (sam) 林東昀 (toung) 張琮翔 (vgod), 朱浩華教授 (hchu)
   2007-2008, 蕭俊宏 (chhsiao) 王尹 (cindylinz), 朱浩華教授 (hchu)
   2008-2009, 王雋伯 (artoo) 蔡鎮宇 (wens), 莊永裕教授 (cyy)
   2009-2010, 李冠毅 (pbrother) 蔡鎮宇 (wens), 莊永裕教授 (cyy)
   2010-2011, 李庭嫣 (lydian) 吳冠龍 (garywgl), 蘇雅韻教授 (yysu)
   2011-2011, 李庭嫣 (lydian) 張秉鈞 (bingjing), 蘇雅韻教授 (yysu)
   2012-2013, 郭至中 (math120908) 王挺宇 (robertabcd), --
   2013-2014, 黃上恩 (tmt514), 薛智文教授 (cwhsueh)
   2014-2015, 簡名沅 (myjian), 薛智文教授 (cwhsueh)
   2015-2016, 吳一德 (wyde), 薛智文教授 (cwhsueh)
   2016-2018, 吳一德 (wyde), 蔡欣穆教授 (hsinmu)
   2018-2020, 系統助教群 (ta217), 蔡欣穆教授 (hsinmu)
   2020-    , 系統助教群 (ta217), 蕭旭君教授 (hchsiao)
```


## 大事記

*   2003.10 – 購入九部 IBM x335 工作站。
*   2004.11 – 購入新的 RAID System，容量達 3.2 TB，用以解決容量吃緊的問題。
*   2004.12 – 購入兩部 IBM x236 中階伺服器，用來更新 NFS 與 Mail server，六部 IBM x336 工作站 (中研院補助)。
*   2005.08 – 新任 217 指導教授由朱浩華教授擔任，並施行雙研究生助教制度。
*   2005.08 – Mail service 從 NFS 獨立出來運作。
*   2006.12 – 購入六部 AMD Dual Core 工作站。
*   2007.03 – 購入四部 IBM x3350 工作站。
*   2007.12 – 購入一部 IBM x3350 工作站。
*   2008.08 – 購入兩部 Dell PowerEdge 1950 III 工作站。
*   2008.10 – 購入兩部 Dell PowerEdge 1950 III 工作站。
*   2008.12 – 購入六部 Dell PowerEdge 1950 III 工作站。
*   2009.09 – 購入兩部 IBM System x3550 工作站。
*   2009.12 – 購入兩部 Dell PowerEdge R710 工作站。
*   2010.05 – R217 汰換桌子，共十六張。
*   2010.06 – 購入六部 Dell PowerEdge R710 工作站。
*   2010.07 – R217 汰換櫃子，換成系統櫃。
*   2010.10 – 新增一部 HP P4015n 印表機。
*   2010.11 – 購入兩部 Dell PowerEdge R610 工作站。
*   2012.09 – 蔡欣穆教授於系上開 NASA 課程，預計培育學生成為系上系統管理與網路管理人才。
*   2014.10 – 主機房整建，合併 R213 與 R215 並採用機櫃式空調。
*   2018.12 – 購入兩部 Asus Esc8000G4  GPU Server工作站。
