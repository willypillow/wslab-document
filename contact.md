# 聯絡助教 Contact

## [工作站管理團隊 (Workstation Team)](./index.md)

![Email\_217ta](https://drive.google.com/uc?export=download&id=1YZeUlAZJJ55UUAphDZdVq2KRYbPcF55_)

-   CSIE Account
-   Mail Server
-   CSIE Info
-   CSIE Esystem
-   Workstation (linux\*, bsd\*) user quota, process management
-   CSIE dept. website maintenance
-   R215 server room issues
-   R204 classroom issues

## [網路管理團隊 (Network Administration Team)](http://nalab.csie.ntu.edu.tw/)

-   IP Address management
-   Domain Name Service management
-   Wired/Wireless network management (error handling, consulting, etc.)

### 指導教授：蕭旭君教授

-   德田館 511 研究室
-   +886-2-3366-4888 ext. 511

### 217 實驗室

-   德田館 217 工作站實驗室
-   +886-2-3366-4888 ext. 217
-   申請系館門禁（請洽德田館 209 系辦葉先生）
