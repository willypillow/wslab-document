# 個人網頁 Homepage

以帳號為 b09902000 為例：（自行設定時請換成自己的帳號）

## 設定 

在個人目錄下進入 (或建立) `htdocs` 目錄。

```shell
$ mkdir ~/htdocs
$ cd ~/htdocs
```
`htdocs` 目錄下的 `index.html` 為系統預設之讀取檔案。

設定完成後，前往 [https://www.csie.ntu.edu.tw/~b07902000/](https://www.csie.ntu.edu.tw/~b09902000/) 就會看到 index.html 的內容，或是如果沒有 index.html 就會是 htdocs 目錄下的內容

## CGI-script

若需要於個人網站上使用 cgi-script，請將 cgi-script 置於 `~/htdocs/cgi-bin` 目錄下，並確保 script 有執行權限：

```shell
$ mkdir -p ~/htdocs/cgi-bin
$ cd ~/htdocs/cgi-bin
```

建立 `hello.pl`：

```shell
$ cd ~/htdocs/cgi-bin
$ vim hello.pl
$ chmod +x hello.pl
```
並寫入以下內容：

```
#!/usr/bin/perl

print "Content-type: text/html\n\n"; 
print "Hello World!\n";
```
接著前往 [https://www.csie.ntu.edu.tw/~b07902000/cgi-bin/hello.pl](https://www.csie.ntu.edu.tw/~b07902000/cgi-bin/hello.pl)，就應該能看到 "Hello World!" 字樣。

## FastCGI 設定

目前預設的 php handler 為 Apache 內建的 Apache 2.0 Handler，因此 php 會以使用者 `apache` 執行。而使用者建立的檔案權限預設為 755，如果需要 php 有使用者檔案的讀寫權限，可以透過設定 FastCGI handler 的方式讓 php 以使用者身份執行，設定方法如下：

先建立 FastCGI wrapper：

```shell
$ vim ~/htdocs/php-fcgi-starter
$ chmod +x ~/htdocs/php-fcgi-starter
```

```shell
#!/bin/sh
PHPRC=/etc
export PHPRC
exec /usr/bin/php-cgi
```

並設定 FastCGI 為 php 的 handler（請將 wrapper 的路徑換成 php-fcgi-starter 的絕對路徑）：

```shell
$ vim ~/htdocs/.htaccess
```

增加以下三行：

```shell
Options +ExecCGI
AddHandler fcgid-script .php
FCGIWrapper /home/student/09/b09902000/htdocs/php-fcgi-starter .php
```

設定好後可以使用以下 php 測試是否設定成功：

```shell
$ echo "<?php echo exec('whoami'); ?>" > ~/htdocs/user.php
```

前往 [https://www.csie.ntu.edu.tw/~b09902000/user.php](https://www.csie.ntu.edu.tw/~b09902000/user.php) 後應該會看到自己的 username 而不是 apache。

## 計數器 Count 2.6 (Faculty Only)

-   在 html 檔裡加上 (請把 USERNAME 換成自己的帳號)
![](https://drive.google.com/uc?export=download&id=1eeFWS74uxQspd9Bg0bNKbFOfhvK2yMIh)

-   [Count 2.6 的詳細說明](http://www.muquit.com/muquit/software/Count/Count2.6/Count.html)
