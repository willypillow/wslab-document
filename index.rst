.. WSLab documentation master file, created by
   sphinx-quickstart on Wed Apr  4 00:40:06 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

NTU CSIE System Administration Team
===================================

.. toctree::
   :titlesonly:
   :caption: 最新公告 (New)
   :glob:

   announcement.md

.. toctree::
   :titlesonly:
   :caption: 工作站規範與教學
   :glob:

   SSH_tutorial.md
   computation_and_usage_rules.md
   tmp2_usage_rules.md
   tips_for_workstation_usage.md
   GPU_server.md
   python_package_installation_tutorial.md
   docker_tutorial.md

.. toctree::
   :titlesonly:
   :caption: 工作站帳號
   :glob:

   account.md
   change_password.md
   forget_password.md
   request_password_reset.md

.. toctree::
   :titlesonly:
   :caption: 系上服務介紹
   :glob:

   mirror_service.md
   MATLAB_R2017a_installation_tutorial.md
   email.md
   email_forwarding.md
   printer.md
   printing_tutorial.md
   homepage.md
   FAQ.md

.. toctree::
   :titlesonly:
   :caption: 使用工作站: 實用軟體介紹
   :glob:

   FTP.md
   FileZilla.md
   WinSCP.md
   run_GUI_application_with_Xming.md
   using_VNC.md

.. toctree::
   :titlesonly:
   :caption: 工作站環境
   :glob:

   hardware.md
   software.md
   operating_system.md
   status.md

.. toctree::
   :titlesonly:
   :caption: 表單與聯絡資訊
   :glob:

   room_215.md
   application_forms.md
   contact.md

.. toctree::
   :titlesonly:
   :caption: 歷史
   :glob:

   history.md
