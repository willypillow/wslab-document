﻿# 作業系統 Operating System
目前工作站提供 ArchLinux 與 FreeBSD 兩種作業系統。其中 linux1~linux15, oasis1~oasis3 為 ArchLinux，bsd1 為 FreeBSD。
* ArchLinux
    * ArchLinux is a lightweight and flexible Linux® distribution that tries to Keep It Simple.
    * ArchLinux is an independently developed, x86-64 general-purpose GNU/Linux distribution that strives to provide the latest stable versions of most software by following a rolling-release model. The default installation is a minimal base system, configured by the user to only add what is purposely required.
* FreeBSD
    * FreeBSD is an advanced operating system for x86 compatible (including Pentium and Athlon), amd64 compatible (including Opteron, Athlon64, and EM64T), UltraSPARC, IA-64, PC-98 and ARM architectures. 
    * It is derived from BSD, the version of UNIX developed at the University of California, Berkeley. It is developed and maintained by a large team of individuals. Additional platforms are in various stages of development.

在使用上有任何疑問，請多參考以下資源：
* ArchLinux wiki: [https://wiki.archlinux.org](https://wiki.archlinux.org)
* FreeBSD wiki: [https://wiki.freebsd.org](https://wiki.freebsd.org)
* 鳥哥的 Linux 私房菜: [http://linux.vbird.org](http://linux.vbird.org)