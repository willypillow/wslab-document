# CSIE 列印教學
## 使用網站列印(Web Print)

我們使用圖形介面 [PaperCut](https://printing.csie.ntu.edu.tw/) 列印。  

### step 1
到 [PaperCut](https://printing.csie.ntu.edu.tw/)。

### step 2
用你的學號以及工作站密碼登入（忘記密碼參考[忘記 CSIE 工作站密碼](./forget_password.md)）。

### step 3
左邊選擇「網路列印」，會看到右邊有「發送工作」。

<center>
![print-tutorial-1](https://drive.google.com/uc?export=view&id=1nsLMMSSQhdOzLn34mxxILFrXmKFHbUpF)
</center>

### step 4
可以選擇印表機。Ariel 及 Elsa 在209辦公室外頭走廊上。選擇完成後按右下角進入下一步。  
<center>
![print-tutorial-2](https://drive.google.com/uc?export=view&id=1Aa4M37I7pJ02-Hu4Jas_tytnx8eO3Ijs)
</center>

### step 5
接著一路照做就可以了。這裡支援的格式有

`pdf`, `bmp`, `dib`, `gif`, `jfif`, `jif`, `jpe`, `jpeg`, `jpg`, `png`, `tif`, `tiff`

## 使用 CutePDF 將 Word 或 Excel 轉成 PDF

如果使用 Word 或 Excel 內建的匯出功能將檔案匯出成為 PDF 時，有可能會變成亂碼。即使在自己的電腦上打開 PDF 顯示正常，從印表機印出來的依然有可能會是亂碼。  
因此，我們必須使用 204 的 CutePDF 將 Word 或 Excel 的工作轉成 PDF 檔案。

### step 0
假設我們今天有一份 Word 檔。  

<center>
![print-tutorial-3](https://drive.google.com/uc?export=view&id=1cueyHbZ5smTa0VntxqEQWIHV1kKpgqOP)
</center>

### step 1
到列印的地方，在印表機的選單裡選擇「CutePDF Writer」。  

<center>
![print-tutorial-4](https://drive.google.com/uc?export=view&id=16SWOo28CE84pQTixpPUsZz78ynAcnSvW)
</center>

### step 2
按下「Print」之後，會出現問你要把 PDF 檔案存在哪裡的視窗。  

<center>
![print-tutorial-5](https://drive.google.com/uc?export=view&id=1bCgFXkizgqayyDmSCFc3iAwluXjo-_9-)
</center>

### step 3
選好自己存放 PDF 的地方以及檔案名稱，就可以得到不會變成亂碼的 PDF 了！

以上是使用 CutePDF 把 Word 或 Excel 轉成 PDF 檔案的介紹。

