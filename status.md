﻿# 系統狀態 Status
你可以在 [monitor.csie.ntu.edu.tw](https://monitor.csie.ntu.edu.tw) 找到 CPU 用量、memory 用量、``/tmp2`` 剩餘容量等資訊；除此之外，你也可以在工作站上使用 ``htop`` 指令來監測單一工作站的使用狀況，決定你要使用哪一台工作站。
