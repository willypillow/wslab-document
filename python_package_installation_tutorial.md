﻿# Python Package 安裝教學 Python Package Installation Tutorial
工作站管理團隊一般只負責管理系統套件，若有任何 Python package 的使用需求，你可以將其安裝在 /tmp2 或家目錄下。
一般在個人機器上的安裝指令為
```
python setup.py install
```
然而此指令可能需要將某些檔案寫入如 /lib 等需要 root 權限方能寫入的目錄，故而你需要將其安裝在 /tmp2 或家目錄等開放一般使用者讀寫之目錄；你可以在指令後加上 `--prefix [path]` 參數來安裝在指定的目錄下，以下是簡單的使用範例：
```
python setup.py install \
–prefix=$HOME/python-packages/package-name
```
其中 `HOME` 參數預設指向使用者的家目錄。
安裝完成後，你還需要設定 `PYTHONPATH` 變數來指定 import 的目錄，簡單的範例如下：
```
export PYTHONPATH=$HOME/python-packages/package-name/lib/python2.7/site-packages/:$PYTHONPATH
```
這樣你就可以順利在程式中 import 該 package 了。你可以將該指令寫在你慣用的 shell 的設定檔中，就不用每次登入都設定一次。