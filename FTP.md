﻿# FTP
由於目前只提供 SSH 安全連線方式。因此 FTP 也全面使用具有安全連線的 SFTP。 在 Windows 上可用 [FileZilla](https://filezilla-project.org/) 或 [WinSCP](https://winscp.net/) 等軟體。也請使用安全性高的密碼。
## 連線方式
* 主機 Host: { linux{1..15}, oasis{1..3}, bsd1 }.csie.ntu.edu.tw
* 協定 Protocol: SFTP - SSH File Transfer Protocol'
* 連接埠 Port: 22 (default) 
<br><br>
關於 FileZilla 與 WinSCP 的設定，請參考網站中的相關教學。
