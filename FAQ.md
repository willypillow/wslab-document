# 常見問題 FAQ

## Q1 畢業生工作站 Quota 縮減

請至 [CSIE Info](https://info.csie.ntu.edu.tw/graduation) 完成手續

## Q2 我想申請工作站帳號

請參考 [帳號](./account.md)。

## Q3 忘記工作站密碼

### Situation 1
若您的 CSIE 工作站帳號為學生帳號，可透過系統自動申請重設密碼，不必寄信。

重設密碼 SOP：

1.  開啟 [esystem 忘記密碼系統](https://esystem.csie.ntu.edu.tw/forget_password)
2.  輸入工作站帳號名稱，按下「Reset Password」送出申請
3.  請稍等 3 分鐘，系統將寄重設確認信到 NTU 信箱，寄件者是 CSIE Esystem <esystem@csie.ntu.edu.tw>，標題為 **\[CSIE Workstation\] password reset**
4.  點選信中的連結，注意連結的網址開頭必須是 https://esystem.csie.ntu.edu.tw/reset_password，**以防遭到釣魚**
5.  網頁會顯示成功重設密碼，並把新密碼寄到 NTU 信箱
6.  使用此密碼登入到工作站或 CSIE Info 更改密碼

詳細內容可參考 [忘記 CSIE 工作站密碼](./forget_password.md)

### Situation2
若您是系友，或 NTU MAIL 已不使用
請參考 [申請重設密碼](./request_password_reset.md)

## Q4 Account

*  Could I name my account after my name for students? <br>
   No, because of management issue. We use a student id as the account name for undergraduate students, graduate students and phd students. <br><br>
*  How many user space I can use? <br>
   For undergraduate and master student, each student has 1G user quota. For alumni, each alumnu has 100M user quota. <br><br>

## Q5 Mail

*  May I put files on mail server? <br>
   No. you cannot. The system automatically removes non-mail files on the server. <br><br>
*  Why I have to enter my password each time in pine or mutt? <br>
   Pine and Mutt access mailbox via IMAP (or POP3) which requires authentication. You may want to use screen to keep pine/mutt open. <br><br>
*  What is `~/mail` and `~/Mail`? <br>  
   `~/Mail` is where Mutt stores mail. `~/mail` is for Pine. <br><br>
*  How to forward my mail? <br>
   Plaese login Webmail and set it in “Preference”. <br><br>
*  What is Greylisting? <br>
   Check this URL for detail. <br><br>

## Q6 Linux

*  I’m using GNOME. Why does it suddenly stop working? <br>
   This usually happens after upgrading to a new GNOME version. You can remove your GNOME related settings by
   ```shell
   cd ~ && rm -rf .gconf* .gnome* .gnomehackrc .recently-used
   ```

**Please backup them before doing this.** <br>
Usually this helps, if not, please contact the TAs.<br>
**Note that you may lose your Firefox or Mozilla settings.**

*  Where is my favorite KDE? <br>
   We no longer support KDE as our desktop environment. Please switch to GNOME. <br><br>
*  Why the outputs from “dict” are broken? <br>
   The output by dict protocol is `UTF-8`. You can use `dictl` which automatically transforms the encoding for you. You have to set `LANG` or `LC_CTYPE` correctly. For example, they should be `zh_TW.Big5` for most of you. <br><br>
*  Can I use X window environment of BSD and Linux boxes on my Windows desktop? <br>
   Yes. You have to start a VNC server on any one of workstations first. Then, use a VNC client to connect. **We strongly recommend you use VNC over an encrypted channel.** To setup, please refer to [vnc-ssh](http://www.csie.ntu.edu.tw/~rafan/vnc-ssh.html) and remember to add `-localhost` when you start the vnc server. <br><br>
*  Why does my mail forwarding not work? <br>
   Please make sure you modify your forwarding setting in the [webmail service](http://webmail.csie.ntu.edu.tw/). If it does not work, please contact the TAs. <br><br>
*  How do I know my home directory space usage? <br>  
   You can use the command `du` to list the space usage. For example,
   * On Linux
        ```shell
        du –max-depth=1 ~ | sort -n
        ```
   * On BSD
        ```shell
        du -d 1 ~ | sort -n
        ```

## Q7 BSD

*  Can we run MATLAB on BSD machines? <br>
   Yes. But this is **EXPERIMENTAL**, not officially supported by MATLAB. <br><br>
*  Can we run linux binary on BSD machines? <br>
   Yes. <br><br>

