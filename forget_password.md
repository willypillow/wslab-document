# 忘記密碼 / Forget Password

## Caution
以下方法需要**你的 CSIE 工作站帳號為學生帳號**而且**你的 NTU MAIL (學號@ntu.edu.tw)仍在使用**，否則請參考[申請重設密碼](./request_password_reset.md)。<br>
This tutorial only for students in school. If you are alumni, please refer to this page.[Request a Password Reset](./request_password_reset.md).

## step 1
如果你忘記 csie 工作站密碼，請到 [esystem](https://esystem.csie.ntu.edu.tw/forget\_password) 申請新密碼。輸入你的 NTU 信箱，點選 RESET PASSWORD。<br>
If you forget csie workstation password. Please go to [esystem](https://esystem.csie.ntu.edu.tw/forget\_password) website register a new password. You’ll see this page then enter your NTU mail address and press Reset Password.

<center>
![Screenshot from 2015-10-30 10:42:36](https://drive.google.com/uc?export=download&id=1J5p_p0IRSJnFlXHs9lQpmlr89tUV9p3Q)
</center>

## step 2
系統會寄一封信到你的 NTU 信箱 <br>
System will send you a mail to your NTU mailbox.

<center>
![Screenshot from 2015-10-30 10:35:29](https://drive.google.com/uc?export=download&id=1XXlkzsITzKGiQ4YLQH-DVtknPr0Ou9uq)
</center>

## step 3
打開NTU信箱，會收到一封 **\[CSIE WORKSTATION\] password reset** 的信。打開信件，點選 URL。<br>
Go to NTU mailbox and open the **\[CSIE WORKSTATION\] password reset** and click on the URL.

<center>
![Screenshot from 2015-10-30](https://drive.google.com/uc?export=download&id=1o2Ak5Ew1oI88a71mClNYGvxx3-Bn1ff9)
</center>

## step 4
就會連到此網頁，代表系統已重置你的密碼，並且把密碼寄到你的信箱。<br> 
If you get this page, means the system reset your password successfully and send the new password to your NTU mailbox.

<center>
![Screenshot from 2015-10-30 10:39:34](https://drive.google.com/uc?export=download&id=1d-WIH-Ak_CLqlPK8zgJa2uaL38Qrt3BS)
</center>

### step 5
收到新密碼。<br>
Receive the new password.

<center>
![Screenshot from 2015-10-30 11:01:52](https://drive.google.com/uc?export=download&id=1Na4JiVffENTovmHcPzBNqCH6cu0NSeAw)
</center>

### step 6
再到 CSIE INFO 去修改密碼 請參考[更改密碼](./change_password.md#csie-info) <br>
Go to CSIE INFO to change your password, you can refer [CHANGE PASSWORD](./change_password.md#csie-info).

<center>
![Screenshot from 2015-10-30 09:36:02](https://drive.google.com/uc?export=download&id=1m0ugpw6TbdUlb3uuYTyllTN_weS9MFQr)
</center>
