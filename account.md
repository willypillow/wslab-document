# 帳號 Account
## 帳號申請流程 Workstation Account Application
**帳號申請單 Account Application Form** 
[下載中文申請單](https://drive.google.com/uc?export=view&id=1IU6O9D-lNWxOYKrFrL1PHhpRxYdipBLi)
[Download English veiirsion](https://drive.google.com/uc?export=view&id=1nsWEGVzriAeTaEYgIVNa3mB0CfqpZBXf)

### Situation 1
系/所教授、職員、專任助教、工讀生 Faculty, Staff, Teaching assistant, Part-time worker

**申請流程 How-to apply**
* 由系/所辦通知工作站實驗室建立帳號<br>
  Your account will be created upon requests from CSIE/GINM office.

### Situation 2
資工系、資工所、網媒所學生 CSIE/GINM Students
**包含大學部（含雙主修、輔系生）、碩士班（一般生、在職生、產業專班）、博士班（一般生、直升)**

**申請流程 How-to apply**
* 於新學期初由工作站實驗室開立帳號後，將密碼寄至臺大信箱<br>
  Your account should have been created and your password should be sent to NTU Mail.
* 若無法登入或忘記密碼，請聯絡[系統管理助教群](./contact.md)。<br>
  If you cannot log in or forget your password, please send an email from your NTU Mail to [System Management TAs](./contact.md) to request a password reset.
    
### Situation 3
外系修課、專題研究學生 Interdepartmental Students enrolled in CSIE/GINM courses or research

**申請流程 How-to apply**
1. 填妥申請書 Fill the application form.
2. 授課教授簽名同意 Request a signature from the course’s instructor.
3. 送交 217 工作站實驗室申請 Submit the form to room 217.

**使用年限 Expiration date**
* 無論是第一或第二學期進行申請，帳號都統一於下一學年(暑假時)刪除。<br>
  Your account will be deleted in the next academic year.

### Situation 4
研究助理、博士後研究 Research assistant, Postdoctoral research

**申請流程 How-to apply**
1. 填妥申請書 Fill the application form.
2. 指導教授簽名同意 Request a signature from your advisor.
3. 送交工作站實驗室申請 Submit the form to room 217.

## 帳號空間限制 Limitation of the account space

* 在校生 Current Student: 2 GB user quota and 1 GB mail quota per user.
* 系友 Alumni: 200 MB user quota and 100 MB mail quota per user.
* 外系修課 Students enrolled in CSIE/GINM courses: 500 MB user quota per user.

## 更改密碼 Change Password

如果需要變更帳號密碼，請參考[更改密碼](./change_password.md)。<br>  
Please reference to the [Change Password](./change_password.md).

## 忘記密碼 Forget Password

請參考[忘記 CSIE 工作站密碼](./forget_password.md)。<br>
Please reference to the [FORGET CSIE WORKSTATION PASSWORD](./forget_password.md).

## 申請重設密碼 Reset Password

請參考[申請重設密碼](./request_password_reset.md)。<br>
Please reference to the [Request a Password Reset](./request_password_reset.md).

## 帳號停權說明 Statement about account suspension

如你有以下狀況, 我們將會依對系統的損害程度, 進行停止所執行的程式或是暫時停權等處份.<br>
If you have any of the following situations, we will suspend your executing programs or your account temporarily according to the extent of damage to the system.

* 執行過多程式導致系統負荷過重  
  Execute so many programs as to overwhelm the system.
    
* 遇重大事件危急系統安全時，未依規定時間內更改密碼者  
  Not changing your password in the given time when there comes a serious problem endangering the system security.
    
* 疑似帳號遭盜用者  
  Your account is accessed by an unauthorized person.

* 更多細節，請參考[工作站使用規範](./computation_and_usage_rules.md)
