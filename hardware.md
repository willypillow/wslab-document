﻿# 硬體 Hardware 
<!-- rst csv-table -->

## 工作站
```eval_rst
.. csv-table:: 
   :header: "Host", "CPU(threads)", "Memory", "OS", "/tmp2"
   :widths: 15, 40, 15, 15, 15

   linux1, "Intel Xeon E5-2620 v4 @ 2.10GHz (32)", 64G, Archlinux, 456G
   linux2, "Intel Xeon E5-2620 v4 @ 2.10GHz (32)", 64G, Archlinux, 456G
   linux3, "Intel Xeon E5-2620 v4 @ 2.10GHz (32)", 64G, Archlinux, 456G
   linux4, "Intel Xeon E5-2620 v4 @ 2.10GHz (32)", 64G, Archlinux, 456G
   linux5, "Intel Xeon E5-2620 0 @ 2.00GHz (24)", 128G, Archlinux, 1004G
   linux6, "Intel Xeon E5-2620 0 @ 2.00GHz (24)", 128G, Archlinux, 1.3T
   linux7, "Intel Xeon E5-2620 0 @ 2.00GHz (24)", 128G, Archlinux, 1004G
   linux8, "Intel Xeon E5-2620 0 @ 2.00GHz (24)", 128G, Archlinux, 1004G
   linux9, "Intel Xeon E5-2620 0 @ 2.00GHz (24)", 128G, Archlinux, 2.6T
   linux10, "Intel Xeon E5-2620 0 @ 2.00GHz (24)", 128G, Archlinux, 3.5T
   linux11, "Intel Xeon E5-2620 0 @ 2.00GHz (24)", 128G, Archlinux, 1004G
   linux12, "Intel Xeon E5-2620 0 @ 2.00GHz (24)", 128G, Archlinux, 5.4T
   linux13, "Intel Xeon E5-2620 0 @ 2.00GHz (24)", 128G, Archlinux, 819G
   linux14, "Intel Xeon E5-2620 0 @ 2.00GHz (24)", 128G, Archlinux, 1004G
   linux15, "Intel Xeon E5630 @ 2.53GHz (16)", 74G, Archlinux, 3.9T
   oasis1, "Intel(R) Xeon(R) E5620 @ 2.40GHz (VM, 4)", 7.6G, Archlinux, 64G
   oasis2, "Intel(R) Xeon(R) E5620 @ 2.40GHz (VM, 4)", 7.6G, Archlinux, 64G
   oasis3, "Intel(R) Xeon(R) E5620 @ 2.40GHz (VM, 4)", 7.6G, Archlinux, 64G 

.. csv-table:: 
   :header: "Host", "CPU(threads)", "GPU", "Memory", "OS", "/tmp2"
   :widths: 15, 37, 15, 10, 15, 8

   meow1, "Intel Xeon GOLD 5118 @ 2.30GHz (24C48T)", GeForce GTX 3090 Ti x4, 64G, Archlinux, 3.5T
   meow2, "Intel Xeon GOLD 5118 @ 2.30GHz (24C48T)", GeForce GTX 1080 Ti x4 + 2080 Ti x4, 64G, Archlinux, 3.5T
```

<!--
| Hostname | CPU(threads) | Memory(GB) |     OS     | /tmp2 size |
|  --------| --------     | --------   | --------  | -------- |
| linux1   | Intel Xeon E5-2620 v4 @ 2.10GHz (32) | 64G     |  ArchLinux        |     456G     |
| linux2   | Intel Xeon E5-2620 v4 @ 2.10GHz (32) | 64G     |  ArchLinux        |     456G     |
| linux3   | Intel Xeon E5-2620 v4 @ 2.10GHz (32) | 64G     |  ArchLinux        |     456G     |
| linux4   | Intel Xeon E5-2620 v4 @ 2.10GHz (32) | 64G     |  ArchLinux        |     456G     |
| linux5   | Intel Xeon E5-2620 0 @ 2.00GHz (24) | 128G     |  ArchLinux        |    1004G      |
| linux6   | Intel Xeon E5-2620 0 @ 2.00GHz (24) | 128G     |  ArchLinux        |    1.3T      |
| linux7   | Intel Xeon E5-2620 0 @ 2.00GHz (24) | 128G     |  ArchLinux        |    1004G      |
| linux8   | Intel Xeon E5-2620 0 @ 2.00GHz (24) | 128G     |  ArchLinux        |    1004G      |
| linux9   | Intel Xeon E5-2620 0 @ 2.00GHz (24) | 128G     |  ArchLinux        |    2.6T      |
| linux10   | Intel Xeon E5-2620 0 @ 2.00GHz (24) | 128G     |  ArchLinux        |   3.5T       |
| linux11   | Intel Xeon E5-2620 0 @ 2.00GHz (24) | 128G     |  ArchLinux        |   1004G       |
| linux12   | Intel Xeon E5-2620 0 @ 2.00GHz (24) | 128G     |  ArchLinux        |    5.4T      |
| linux13   | Intel Xeon E5-2620 0 @ 2.00GHz (24) | 128G     |  ArchLinux        |   819G       |
| linux14   | Intel Xeon E5-2620 0 @ 2.00GHz (24) | 128G     |  ArchLinux        |   1004G       |
| linux15   | Intel Xeon E5630 @ 2.53GHz (16) | 74G     |  ArchLinux        |    3.9T      |
| oasis1   | Intel(R) Xeon(R) E5620 @ 2.40GHz (VM, 4) | 7.6G | ArchLinux | 64G |
| oasis2   | Intel(R) Xeon(R) E5620 @ 2.40GHz (VM, 4) | 7.6G | ArchLinux | 64G |
| oasis3   | Intel(R) Xeon(R) E5620 @ 2.40GHz (VM, 4) | 7.6G | ArchLinux | 64G |
-->

## 印表機 
* Elsa: LaserJet 600 M603 
* Ariel: Sharp MX-M362N

