# 列印 Printer

目前使用 [PaperCut 網頁列印](https://printing.csie.ntu.edu.tw)，使用請參照 [CSIE 列印教學](./printing_tutorial.md)

## 列印之前

請想想你會砍掉幾顆樹。 請不要利用印表機列印任何與課業無關的東西。 印大檔案時(超過 50 頁)請為其他人想想，最好在無人使用的冷門時段才印。

## 列印服務

* 印表機位置

在209辦公室外頭走廊，目前有兩台印表機，Ariel 和 Elsa。

* 可列印的檔案格式

``pdf``, ``bmp``, ``dib``, ``gif``, ``jfif``, ``jif``, ``jpe``, ``jpeg``, ``jpg``, ``png``, ``tif``, ``tiff``  

* 其他檔案

對於不可直接列印的檔案，必須透過適當的程式**轉換成 PostScript 後**，才能印出來喔。
為求最佳的列印效果，除 PostScript 以外，建議先用閱讀程式打開後再列印。

## 網頁列印 Q & A

* Q1.上傳 PDF 後一直轉圈圈，沒印出來也沒被扣款?  

可能是系統無法解析檔名，請將檔名改為 123.pdf 後重試。

* Q2.印出來的英文正常，但中文字體破損、重疊，像印表機碳粉不足(如下圖)?  

<center>![](https://drive.google.com/uc?export=view&id=1fMxQOAgJfO1ren2i7jHXsJ2gUo03NctB)</center>

這是因為系統無法正確解析標楷體，解決方式為把要列印的 PDF 檔用其他 PDF 列印軟體( Ex. CutePDF Writer )先印一次，所產生的新 PDF 檔即可正常列印。參考 [CSIE 列印教學]

* Q3. Quota 不足?

每人每學期有初始 500 quota 的限制，為了提醒大家愛護紙張的重要性，217 不提供增加 quota 的服務。希望大家列印時多想想，是不是真的有需要印出來，是不是有更省紙的印法，愛護地球人人有責！

* Q4.印表機無法解讀?

有些程式生出來的 PostScript，印表機無法正常解讀，導致無法列印 
如果碰到這種情形，可以用下列的指令處理 
```
ps2ps [old.ps] [new.ps]
``` 
然後再試著印 `[new.ps]` 。
  
